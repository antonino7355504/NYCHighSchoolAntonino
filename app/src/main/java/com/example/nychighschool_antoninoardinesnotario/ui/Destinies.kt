package com.example.nychighschool_antoninoardinesnotario.ui

const val HOME = "home"
const val SCHOOL_DETAIL = "schooldetail"
const val SCHOOL_DETAIL_ARG = "school"
const val SCHOOL_NAV = "$SCHOOL_DETAIL/{$SCHOOL_DETAIL_ARG}"