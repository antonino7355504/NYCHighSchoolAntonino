package com.example.nychighschool_antoninoardinesnotario.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.nychighschool_antoninoardinesnotario.R
import com.example.nychighschool_antoninoardinesnotario.model.NYCSchoolData
import com.example.nychighschool_antoninoardinesnotario.model.UIState
import com.example.nychighschool_antoninoardinesnotario.viewmodel.SchoolViewModel

@Composable
fun SchoolDetailScreen(schoolDetailsDBN: String) {
    val viewModel: SchoolViewModel = viewModel()
    viewModel.schoolDataState.observeAsState().value?.let {schoolDetails ->
        when (schoolDetails) {
            is UIState.Success -> {
                schoolDetails.schoolData.firstOrNull()?.let {
                    SchoolDetail(it)
                } ?: SchoolListFailure(errorMessage = "No data available")
            }
            is UIState.Failure -> {
                SchoolListFailure(errorMessage = schoolDetails.errorMessage)
            }
        }
    }
}

@Composable
fun SchoolDetail(schoolDetails: NYCSchoolData) {
    Box(contentAlignment = Alignment.Center) {
        Card(modifier = Modifier.padding(8.dp)) {
            Column(modifier = Modifier.fillMaxSize()){
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceAround,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = stringResource(id = R.string.school_dbn, schoolDetails.dbn))
                    Text(
                        text = stringResource(id = R.string.school_name, schoolDetails.school_name)
                    )
                }
                Text(text = stringResource(
                    id = R.string.school_neigh,
                    schoolDetails.neighborhood
                ))
                Text(text = schoolDetails.overview_paragraph)
            }
        }
    }
}
