package com.example.nychighschool_antoninoardinesnotario.ui.screens

import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.nychighschool_antoninoardinesnotario.R
import com.example.nychighschool_antoninoardinesnotario.model.NYCSchoolData
import com.example.nychighschool_antoninoardinesnotario.model.UIState
import com.example.nychighschool_antoninoardinesnotario.ui.SCHOOL_DETAIL
import com.example.nychighschool_antoninoardinesnotario.ui.SCHOOL_DETAIL_ARG
import com.example.nychighschool_antoninoardinesnotario.viewmodel.SchoolViewModel

@Composable
fun SchoolListScreen(navController: NavController) {
    val viewModel: SchoolViewModel = viewModel()
    viewModel.schoolDataState.observeAsState().value?.let { schoolDataState ->
        when (schoolDataState) {
            is UIState.Success -> {
                SchoolList(schoolDataState.schoolData) {
                    navController.navigate("$SCHOOL_DETAIL/$it")
                }
            }
            is UIState.Failure -> {
                SchoolListFailure(schoolDataState.errorMessage)
            }
        }
    }
}

@Composable
fun SchoolListFailure(errorMessage: String) {
    val context = LocalContext.current
    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
}

@Composable
fun SchoolList(schoolData: List<NYCSchoolData>, openDetails: (NYCSchoolData) -> Unit) {
    LazyColumn {
        items(schoolData) {
            SchoolItem(it, openDetails)
        }
    }
}

@Composable
fun SchoolItem(schoolItem: NYCSchoolData, openDetails: (NYCSchoolData) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxHeight()
            .padding(8.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = stringResource(id = R.string.school_dbn, schoolItem.dbn))
            Text(
                modifier = Modifier
                    .clickable {
                        openDetails(schoolItem)
                    }
                    .padding(4.dp),
                text = stringResource(id = R.string.school_name, schoolItem.school_name))
        }
    }
}
