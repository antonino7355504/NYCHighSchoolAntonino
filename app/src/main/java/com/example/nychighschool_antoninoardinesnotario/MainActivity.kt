package com.example.nychighschool_antoninoardinesnotario

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgs
import androidx.navigation.navArgument
import com.example.nychighschool_antoninoardinesnotario.model.NYCSchoolData
import com.example.nychighschool_antoninoardinesnotario.ui.HOME
import com.example.nychighschool_antoninoardinesnotario.ui.SCHOOL_DETAIL_ARG
import com.example.nychighschool_antoninoardinesnotario.ui.SCHOOL_NAV
import com.example.nychighschool_antoninoardinesnotario.ui.screens.SchoolDetailScreen
import com.example.nychighschool_antoninoardinesnotario.ui.screens.SchoolListScreen
import com.example.nychighschool_antoninoardinesnotario.ui.theme.NYCHighSchool_AntoninoArdinesNotarioTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NYCHighSchool_AntoninoArdinesNotarioTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = HOME) {
                        composable(route = HOME) {
                            SchoolListScreen(navController = navController)
                        }
                        composable(route = SCHOOL_NAV,
                            arguments = listOf(navArgument(SCHOOL_DETAIL_ARG) {
                                type = NavType.StringType
                            })
                        ) { navBackStackEntry ->
                            navBackStackEntry
                                .arguments?.getString(SCHOOL_DETAIL_ARG)?.let { dbn ->
                                    SchoolDetailScreen(schoolDetailsDBN = dbn)
                                }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NYCHighSchool_AntoninoArdinesNotarioTheme {
        Greeting("Android")
    }
}