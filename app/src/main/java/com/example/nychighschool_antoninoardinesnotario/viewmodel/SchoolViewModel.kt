package com.example.nychighschool_antoninoardinesnotario.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nychighschool_antoninoardinesnotario.model.IRepository
import com.example.nychighschool_antoninoardinesnotario.model.RepositoryImpl
import com.example.nychighschool_antoninoardinesnotario.model.UIState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SchoolViewModel: ViewModel() {

    private val _schoolDataState = MutableLiveData<UIState>()
    val schoolDataState: LiveData<UIState>
    get() = _schoolDataState

    private val repository: IRepository by lazy {
        RepositoryImpl()
    }

    init {
        fetchSchoolData()
    }

    private fun fetchSchoolData() {
        viewModelScope.launch {
            repository.getAllSchools().collect { schoolData ->
                _schoolDataState.value = schoolData
            }
        }
    }
}