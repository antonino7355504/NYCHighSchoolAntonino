package com.example.nychighschool_antoninoardinesnotario.model.remote

import com.example.nychighschool_antoninoardinesnotario.model.NYCSchoolData
import retrofit2.Response
import retrofit2.http.GET

interface Service {
    @GET(ENDPOINT)
    suspend fun getNYCSchools(): Response<List<NYCSchoolData>>
}