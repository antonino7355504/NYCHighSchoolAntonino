package com.example.nychighschool_antoninoardinesnotario.model

import kotlinx.coroutines.flow.Flow

interface IRepository {
    fun getAllSchools(): Flow<UIState>
    fun getSchoolByDBN(dbn: String): Flow<UIState>
}