package com.example.nychighschool_antoninoardinesnotario.model.remote

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class Network {

    val service: Service by lazy {
        initRetrofit()
    }

    private fun initRetrofit() =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(Service::class.java)
}