package com.example.nychighschool_antoninoardinesnotario.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NYCSchoolData(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
    val neighborhood: String
): Parcelable
