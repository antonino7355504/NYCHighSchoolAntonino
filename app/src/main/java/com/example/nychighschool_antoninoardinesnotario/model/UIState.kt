package com.example.nychighschool_antoninoardinesnotario.model

sealed class UIState {
    data class Success(val schoolData: List<NYCSchoolData>): UIState()
    data class Failure(val errorMessage: String): UIState()
}
