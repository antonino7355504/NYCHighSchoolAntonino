package com.example.nychighschool_antoninoardinesnotario.model

import com.example.nychighschool_antoninoardinesnotario.model.remote.Network
import com.example.nychighschool_antoninoardinesnotario.model.remote.Service
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RepositoryImpl: IRepository {

    private val service: Service by lazy {
        Network().service
    }

    private var inMemoryCache = listOf<NYCSchoolData>()

    override fun getAllSchools(): Flow<UIState> {
        return flow {
            val response = service.getNYCSchools()
            if (response.isSuccessful) {
                response.body()?.let { schoolData ->
                    inMemoryCache = schoolData
                    emit(UIState.Success(schoolData))
                } ?: kotlin.run {
                    emit(UIState.Failure(response.message()))
                }
            } else {
                emit(UIState.Failure(response.message()))
            }
        }
    }

    override fun getSchoolByDBN(dbn: String): Flow<UIState> {
        return flow {
            if (inMemoryCache.isEmpty()) {
                emit(UIState.Failure("No cache data"))
            } else {
                emit(UIState.Success(
                    inMemoryCache.filter { it.dbn == dbn }
                ))
            }
        }
    }


}