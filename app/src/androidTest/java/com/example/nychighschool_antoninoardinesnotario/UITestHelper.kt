package com.example.nychighschool_antoninoardinesnotario

import com.example.nychighschool_antoninoardinesnotario.model.NYCSchoolData

fun fakeDataGenerator() =
    listOf(
        NYCSchoolData(
            "02M260",
            "Clinton School Writers & Artists, M.S. 260",
            "Some large text",
            "Chelsea-Union Sq"
        ),
        NYCSchoolData(
            "21K728",
            "Liberation Diploma Plus High School",
            "Some large text",
            "Chelsea-Union Sq"
        ),
        NYCSchoolData(
            "08X282",
            "Women's Academy of Excellence",
            "Some large text",
            "Chelsea-Union Sq"
        )
    )