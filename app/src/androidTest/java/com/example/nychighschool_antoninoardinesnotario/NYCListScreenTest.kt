package com.example.nychighschool_antoninoardinesnotario

import androidx.activity.compose.setContent
import androidx.compose.ui.test.assert
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.onRoot
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.printToLog
import com.example.nychighschool_antoninoardinesnotario.ui.screens.SchoolList
import com.example.nychighschool_antoninoardinesnotario.ui.theme.NYCHighSchool_AntoninoArdinesNotarioTheme
import org.junit.Rule
import org.junit.Test

class NYCListScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @get:Rule
    val composeTestRuleActivity = createAndroidComposeRule<MainActivity>()

    @Test
    fun whenHavingASchoolDataIsDisplayed() {
        composeTestRuleActivity.activity.setContent {
            NYCHighSchool_AntoninoArdinesNotarioTheme {
                SchoolList(schoolData = fakeDataGenerator()) {

                }
            }
        }
        composeTestRuleActivity
            .onNode(hasText("DBN"))
            .assert(hasText("DBN: 02M260"))
            .assertExists()
        composeTestRuleActivity.onRoot().printToLog("TAG")
    }
}